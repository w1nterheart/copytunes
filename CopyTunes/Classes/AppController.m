//
//  AppController.m
//  CopyTunes
//
//  Created by Vitalii Iroshnikov on 9/27/15.
//  Copyright © 2015 Vitaliy Lopatkin. All rights reserved.
//

#import "AppController.h"

@interface AppController()

@end

@implementation AppController

/* init
 * Class instance initialisation.
 */
-(id)init
{
    if ((self = [super init]) != nil)
    {
        
    }
    return self;
}


/* awakeFromNib
 * Do all the stuff that only makes sense after our NIB has been loaded and connected.
 */
-(void)awakeFromNib
{
    
#if ( MAC_OS_X_VERSION_MAX_ALLOWED < 1070 && !defined(NSWindowCollectionBehaviorFullScreenPrimary) )
    enum {
        NSWindowCollectionBehaviorFullScreenPrimary = (1 << 7)
    };
#endif
    homeViewController = [[HomeViewController alloc] initWithNibName:nil bundle:nil];
    //Enable FullScreen Support if we are on Lion 10.7.x
    [mainWindow setCollectionBehavior:NSWindowCollectionBehaviorFullScreenPrimary];
    
    // Set the delegates and title
    [mainWindow setDelegate:self];
    [mainWindow setTitle:@"Copy Tunes"];
    [[NSApplication sharedApplication] setDelegate:self];
    [mainWindow setAllowsConcurrentViewDrawing:YES];
    [[NSApplication sharedApplication] addWindowsItem:mainWindow title:@"allo" filename:NO];
    [mainWindow setViewsNeedDisplay: YES];
//    [mainWindow.contentView addSubview: homeViewController.view];
    mainWindow.contentViewController = homeViewController;
}


/* setLayout
 * Changes the layout of the panes.
 */
-(void)setLayout:(int)newLayout withRefresh:(BOOL)refreshFlag
{
    BOOL visibleFilterBar = NO;
    // Turn off the filter bar when switching layouts. This is simpler than
    // trying to graft it onto the new layout.
//    if ([self isFilterBarVisible]) {
//        visibleFilterBar = YES;
//        [self setPersistedFilterBarState:NO withAnimation:NO];
//    }
//    
//    [articleController setLayout:newLayout];
//    if (refreshFlag)
//        [[articleController mainArticleView] refreshFolder:MA_Refresh_RedrawList];
//    [browserView setPrimaryTabItemView:[articleController mainArticleView]];
//    //restore filter bar state if necessary
//    if (visibleFilterBar)
//        [self setPersistedFilterBarState:YES withAnimation:NO];
//    [self updateSearchPlaceholderAndSearchMethod];
//    [[foldersTree mainView] setNextKeyView:[[browserView primaryTabItemView] mainView]];
}



- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
//    [[NSApplication sharedApplication] mainWindow].windowController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:[NSBundle mainBundle]];
    
    // Create the toolbar.
    NSToolbar * toolbar = [[NSToolbar alloc] initWithIdentifier:@"MA_Toolbar"];
    [toolbar setDelegate:self];
    [toolbar setAllowsUserCustomization: YES];
    [toolbar setAutosavesConfiguration: YES];
    [toolbar setDisplayMode:NSToolbarDisplayModeIconOnly];
    [mainWindow setToolbar: toolbar];
    
    [self showMainWindow:self];
}

/* showMainWindow
 * Display the main window.
 */
-(IBAction)showMainWindow:(id)sender
{
    [mainWindow makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
