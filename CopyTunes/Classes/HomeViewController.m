//
//  HomeViewController.m
//  CopyTunes
//
//  Created by Vitalii Iroshnikov on 9/27/15.
//  Copyright © 2015 Vitaliy Lopatkin. All rights reserved.
//

#import "HomeViewController.h"
#import <iTunesLibrary/ITLibrary.h>
#import "iTunes.h"

#define NAVIGATION_BUTTON_MARGIN ( 5.f )
#define NAVIGATION_ICON_BUTTON_SIDE ( 44.f - NAVIGATION_BUTTON_MARGIN * 2)
#define NAVIGATION_CANCEL_BUTTON_WIDTH ( 60.f )

#define FRAME_WIDTH (480.f)
#define FRAME_HEIGHT (400.f)

#define HISTORY_LABEL_OFFSET_VERTICAL (282.f)
#define HISTORY_LABEL_OFFSET_HORIZONTAL (18.f)
#define HISTORY_LABEL_WIDTH (92.f)
#define HISTORY_LABEL_HEIGHT (17.f)

#define CURRENT_SONG_LABEL_OFFSET_VERTICAL (339.f)
#define CURRENT_SONG_LABEL_OFFSET_HORIZONTAL (18.f)
#define CURRENT_SONG_LABEL_WIDTH (92.f)
#define CURRENT_SONG_LABEL_HEIGHT (17.f)

#define PROGRESS_INDICATOR_OFFSET_VERTICAL (339.f)
#define PROGRESS_INDICATOR_OFFSET_HORIZONTAL (444.f)
#define PROGRESS_INDICATOR_WIDTH (16.f)
#define PROGRESS_INDICATOR_HEIGHT (16.f)

#define CURRENT_SONG_TEXTFIELD_OFFSET_VERTICAL (339.f)
#define CURRENT_SONG_TEXTFIELD_OFFSET_HORIZONTAL (116.f)
#define CURRENT_SONG_TEXTFIELD_WIDTH (322.f)
#define CURRENT_SONG_TEXTFIELD_HEIGHT (22.f)

#define CHECKBOX_BUTTON_OFFSET_VERTICAL (271.f)
#define CHECKBOX_BUTTON_OFFSET_HORIZONTAL (18.f)
#define CHECKBOX_BUTTON_WIDTH (246.f)
#define CHECKBOX_BUTTON_HEIGHT (32.f)

#define REFRESH_BUTTON_OFFSET_VERTICAL (271.f)
#define REFRESH_BUTTON_OFFSET_HORIZONTAL (270.f)
#define REFRESH_BUTTON_WIDTH (82.f)
#define REFRESH_BUTTON_HEIGHT (32.f)

#define CLEAR_BUTTON_OFFSET_VERTICAL (271.f)
#define CLEAR_BUTTON_OFFSET_HORIZONTAL (362.f)
#define CLEAR_BUTTON_WIDTH (82.f)
#define CLEAR_BUTTON_HEIGHT (32.f)

#define SEARCH_BUTTON_OFFSET_VERTICAL (19.f)
#define SEARCH_BUTTON_OFFSET_HORIZONTAL (280.f)
#define SEARCH_BUTTON_WIDTH (164.f)
#define SEARCH_BUTTON_HEIGHT (32.f)

#define SEARCH_FIELD_OFFSET_VERTICAL (239.f)
#define SEARCH_FIELD_OFFSET_HORIZONTAL (18.f)
#define SEARCH_FIELD_WIDTH (426.f)
#define SEARCH_FIELD_HEIGHT (22.f)

#define HISTORY_TABLE_OFFSET_VERTICAL (65.f)
#define HISTORY_TABLE_OFFSET_HORIZONTAL (18.f)
#define HISTORY_TABLE_WIDTH (426.f)
#define HISTORY_TABLE_HEIGHT (164.f)

@interface HomeViewController () <NSTableViewDataSource, NSTableViewDelegate> {
    NSView *_searchView;
    
    NSTextView *_currentSongLabel;
    NSTextView *_historyLabel;
    
    NSProgressIndicator *_progressIndicator;
    NSTextField *_currentSongTextField;

    NSButton *_checkSearchWhilePlaying;
    NSButton *_refreshButton;
    NSButton *_clearButton;
    NSButton *_searchButton;
    NSSearchField *_searchField;
    
    NSTableView *_historyTableView;
    NSMutableArray *_data;

    BOOL _isUpadtingStatus;
    
    iTunesApplication *_iTunesApp;
    iTunesTrack *_currentTrack;
    
    NSUserNotificationCenter *_notificationCenter;
}

@end
@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self iTunesInitLibrary];
    [self iTunesInitNotificationCenterListener];
    
    _notificationCenter = [NSUserNotificationCenter defaultUserNotificationCenter];
    
//    _currentSongLabel = [[NSTextView alloc] initWithFrame:CGRectMake(CURRENT_SONG_TEXTFIELD_OFFSET_HORIZONTAL, CURRENT_SONG_TEXTFIELD_OFFSET_VERTICAL, CURRENT_SONG_TEXTFIELD_WIDTH, CURRENT_SONG_TEXTFIELD_HEIGHT)];
//    [_currentSongLabel setEditable: NO];
//    [self.view addSubview:_currentSongLabel];

//    _historyLabel = [[NSTextView alloc] initWithFrame:CGRectMake(HISTORY_LABEL_OFFSET_HORIZONTAL, HISTORY_LABEL_OFFSET_VERTICAL, HISTORY_LABEL_WIDTH, HISTORY_LABEL_HEIGHT)];
//    [_historyLabel setEditable: NO];
//    [self.view addSubview:_historyLabel];
    
    _progressIndicator = [[NSProgressIndicator alloc] initWithFrame:CGRectMake(PROGRESS_INDICATOR_OFFSET_HORIZONTAL, PROGRESS_INDICATOR_OFFSET_VERTICAL, PROGRESS_INDICATOR_WIDTH, PROGRESS_INDICATOR_HEIGHT)];
    _progressIndicator.style = NSProgressIndicatorSpinningStyle;
    [_progressIndicator startAnimation: self];
    [self.view addSubview:_progressIndicator];
    
    _currentSongTextField = [[NSTextField alloc] initWithFrame:CGRectMake(CURRENT_SONG_TEXTFIELD_OFFSET_HORIZONTAL, CURRENT_SONG_TEXTFIELD_OFFSET_VERTICAL, CURRENT_SONG_TEXTFIELD_WIDTH, CURRENT_SONG_TEXTFIELD_HEIGHT)];
    [self.view addSubview:_currentSongTextField];
    
    _checkSearchWhilePlaying = [[NSButton alloc] initWithFrame:CGRectMake(CHECKBOX_BUTTON_OFFSET_HORIZONTAL, CHECKBOX_BUTTON_OFFSET_VERTICAL, CHECKBOX_BUTTON_WIDTH, CHECKBOX_BUTTON_HEIGHT)];
    _checkSearchWhilePlaying.title = @"Save track names while iTunes playing";
    [_checkSearchWhilePlaying setButtonType: NSSwitchButton];
    [_checkSearchWhilePlaying setTarget: self];
    [_checkSearchWhilePlaying setAction: @selector(searchCheckboxChanged:)];
    [self.view addSubview:_checkSearchWhilePlaying];
    
    _refreshButton = [[NSButton alloc] initWithFrame:CGRectMake(REFRESH_BUTTON_OFFSET_HORIZONTAL, REFRESH_BUTTON_OFFSET_VERTICAL, REFRESH_BUTTON_WIDTH, REFRESH_BUTTON_HEIGHT)];
    _refreshButton.title = @"Refresh";
    [_refreshButton setButtonType: NSMomentaryPushInButton];
    [_refreshButton setBordered:YES];
    [_refreshButton setBezelStyle:NSRoundedBezelStyle];
    [_refreshButton setTarget: self];
    [_refreshButton setAction: @selector(refreshButtonClicked:)];
    [self.view addSubview:_refreshButton];
    
    _clearButton = [[NSButton alloc] initWithFrame:CGRectMake(CLEAR_BUTTON_OFFSET_HORIZONTAL, CLEAR_BUTTON_OFFSET_VERTICAL, CLEAR_BUTTON_WIDTH, CLEAR_BUTTON_HEIGHT)];
    _clearButton.title = @"Clear";
    [_clearButton setButtonType: NSMomentaryPushInButton];
    [_clearButton setBordered:YES];
    [_clearButton setBezelStyle:NSRoundedBezelStyle];
    [_clearButton setTarget: self];
    [_clearButton setAction: @selector(clearButtonClicked:)];
    [self.view addSubview:_clearButton];
    
    _searchButton = [[NSButton alloc] initWithFrame:CGRectMake(SEARCH_BUTTON_OFFSET_HORIZONTAL, SEARCH_BUTTON_OFFSET_VERTICAL, SEARCH_BUTTON_WIDTH, SEARCH_BUTTON_HEIGHT)];
    _searchButton.title = @"Search";
    [_searchButton setButtonType: NSMomentaryPushInButton];
    [_searchButton setBordered:YES];
    [_searchButton setBezelStyle:NSRoundedBezelStyle];
    [_searchButton setTarget: self];
    [_searchButton setAction: @selector(searchButtonClicked:)];
    [self.view addSubview:_searchButton];
    
    _searchField = [[NSSearchField alloc] initWithFrame:CGRectMake(SEARCH_FIELD_OFFSET_HORIZONTAL, SEARCH_FIELD_OFFSET_VERTICAL, SEARCH_FIELD_WIDTH, SEARCH_FIELD_HEIGHT)];
    [self.view addSubview:_searchField];
    
    _historyTableView = [[NSTableView alloc] initWithFrame:CGRectMake(HISTORY_TABLE_OFFSET_HORIZONTAL, HISTORY_TABLE_OFFSET_VERTICAL, HISTORY_TABLE_WIDTH, HISTORY_TABLE_HEIGHT)];
    [self.view addSubview:_historyTableView];
}

- (void) iTunesNotificationReceived: (NSNotification *)notification {
    NSLog(@"notification payload: %@", notification.userInfo);
    
    //    "Display Line 1" = "Ariana Grande - One Last Time (Marshmello Remix)";
    if ([_iTunesApp isRunning]) {
        _currentTrack = [[_iTunesApp currentTrack] get];
        NSString *name;
        NSString *artist;
        
        //if this is nil then it's probably badly tagged track which is unneeded or
        // a radio station
        if ([_currentTrack artist].length == 0 && [_currentTrack name].length == 0) {
            if ([notification.userInfo objectForKey:@"Display Line 1"] != nil) {
                artist = [notification.userInfo objectForKey:@"Display Line 1"];
                name = [notification.userInfo objectForKey:@"Display Line 1"];
                
                _currentTrack.name = name;
                _currentTrack.artist = artist;
            } else {
                //All hope is lost for this one
                return;
            }
        } else {
            name = [_currentTrack name];
            artist = [_currentTrack artist];
        }
        
        if (name != nil && artist != nil) {
            //TODO: write to DB for accessing it within history with TIMESTAMP of the event
            [self iTunesWriteToDatabase: artist name:name];
        }
        
        if (_notificationCenter != nil) {
            NSUserNotification *notification = [[NSUserNotification alloc] init];
            [notification setTitle: artist];
            [notification setInformativeText: name];
            
            [notification setDeliveryDate:[NSDate dateWithTimeInterval:2 sinceDate:[NSDate date]]];
            //        [notification setSoundName:NSUserNotificationDefaultSoundName];
            [_notificationCenter scheduleNotification:notification];
        }
    }
}

- (void) iTunesWriteToDatabase: (NSString *) artist name:(NSString *) name  {
    NSLog(@"iTunesWriteToDatabase artist: %@ name: %@", artist, name);
}

- (void) iTunesInitNotificationCenterListener {
    if (_iTunesApp != nil && [_iTunesApp isRunning]) {
        NSDistributedNotificationCenter *dnc = [NSDistributedNotificationCenter defaultCenter];
        
        [dnc addObserver:self
                selector:@selector(iTunesNotificationReceived:)
                    name:@"com.apple.iTunes.playerInfo"
                  object:nil];
        
    }
}

- (void) iTunesInitLibrary {
    _iTunesApp = ((iTunesApplication*) [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"]);
    
//    NSError *error = nil;
//    ITLibrary *library = [ITLibrary libraryWithAPIVersion:@"1.0" error:&error];
//    if (library)
//    {
//        NSArray *playlists = library.allPlaylists; //  <- NSArray of ITLibPlaylist
//        NSArray *tracks = library.allMediaItems; //  <- NSArray of ITLibMediaItem
//        
//    }
}

- (void)iTunesGetCurrentSong {
    if (_iTunesApp == nil) {
        [self iTunesInitLibrary];
    }
    
    
//    iTunesApplication *iTunes = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
//    if ( [iTunes isRunning] ) {
//        int rampVolume, originalVolume;
//        originalVolume = [iTunes soundVolume];
//        
//        [iTunes setSoundVolume:0];
//        [iTunes playOnce:NO];
//        
//        for (rampVolume = 0; rampVolume < originalVolume; rampVolume += originalVolume / 16) {
//            [iTunes setSoundVolume: rampVolume];
//            /* pause 1/10th of a second (100,000 microseconds) between adjustments. */
//            usleep(100000);
//        }
//        [iTunes setSoundVolume:originalVolume];
//    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    
    // Update the view, if already loaded.
}

- (void) dealloc {
    NSDistributedNotificationCenter *dnc = [NSDistributedNotificationCenter defaultCenter];
    
    [dnc removeObserver:self name:@"com.apple.iTunes.playerInfo" object:nil];
    [dnc removeObserver:self name:@"com.spotify.client.PlaybackStateChanged" object:nil];
}


@end
