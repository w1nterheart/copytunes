//
//  main.m
//  CopyTunes
//
//  Created by Vitalii Iroshnikov on 9/27/15.
//  Copyright © 2015 Vitaliy Lopatkin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
