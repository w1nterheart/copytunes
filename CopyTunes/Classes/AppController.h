//
//  AppController.h
//  CopyTunes
//
//  Created by Vitalii Iroshnikov on 9/27/15.
//  Copyright © 2015 Vitaliy Lopatkin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include "HomeViewController.h"

@interface AppController : NSObject <NSApplicationDelegate, NSWindowDelegate,NSToolbarDelegate,NSSplitViewDelegate,NSMenuDelegate>
{
    IBOutlet NSWindow * mainWindow;
    IBOutlet HomeViewController * homeViewController;
}
@end

